package com.gustavo.autorizador.feingClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gustavo.autorizador.entities.Usuario;

@Component
@FeignClient(name="rh-usuario", url="", path="/usuario/") 
public interface UsuarioFeingClient {

	@RequestMapping(method = RequestMethod.GET, value = "/buscar") //@GetMapping(value = "/{id}")
	public ResponseEntity<Usuario> findByEmail(@RequestParam String email);
}
