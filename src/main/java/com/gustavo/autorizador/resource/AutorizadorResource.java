package com.gustavo.autorizador.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.autorizador.entities.Usuario;
import com.gustavo.autorizador.services.UsuarioService;

@RestController
@RequestMapping(value="/autorizador")
public class AutorizadorResource {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping(value="/buscar")
	public ResponseEntity<Usuario> findByEmail(@RequestParam String email){
		
		Usuario u = usuarioService.findByEmail(email);
		return ResponseEntity.ok(u);
		
	}
	
}
