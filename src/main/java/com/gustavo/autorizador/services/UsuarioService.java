package com.gustavo.autorizador.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gustavo.autorizador.entities.Usuario;
import com.gustavo.autorizador.feingClient.UsuarioFeingClient;

@Service
public class UsuarioService implements UserDetailsService{

	
	@Autowired
	private UsuarioFeingClient usuarioFeingClient;
	
	public Usuario findByEmail(String email) {
		Usuario usu = usuarioFeingClient.findByEmail(email).getBody();
		return usu;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usu = usuarioFeingClient.findByEmail(username).getBody();
		if (usu == null) throw new UsernameNotFoundException("Usuarie nao encontradis");
		return usu;
	}
}
