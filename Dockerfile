FROM openjdk:11
VOLUME /tmp
ADD ./target/rh-autorizador-0.0.1-SNAPSHOT.jar rh-autorizador.jar
ENTRYPOINT ["java","-jar","/rh-autorizador.jar"]
